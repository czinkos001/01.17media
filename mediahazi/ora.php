<!DOCTYPE html>
<html  lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <meta charset="UTF-8">
    <title>OkosEszközök</title>
</head>

<body style="background:gray" >
<div class="navbar">
    <?php require_once ("menu.php") ?>
</div>
<div class="list-group-item-heading text-center col-sm-8 col-lg-8 col-md-8 col-xs-8">
    <h1 style="color: white">
        Okosóra
    </h1>
    <img  class="col-md-6 col-lg-6 col-sm-6" src="pict/smartwatch.jpg">
    <div>
    <h2>
        Funkció
    </h2>
    <p>
        A legfontosabb funkciója az üzenet, e-mail és híváskezelés. Nagyon sokunk munkája megköveteli a folyamatos kapcsolattartást, egész nap telefonálunk és üzengetünk.
        Az óra ezt hívatott egyszerűbbé tenni a számodra. Bluetooth-on keresztül csatlakoztatható a telefonodhoz, ezután egyből elérhetőek a különböző funkciói,
        egy alkalmazás letöltése után pedig további eszközöket vehetünk igénybe az óránkon.
    </p>
    </div>
    <div>
        <h2>
            Az óra, mint önálló eszköz
        </h2>
        <p>Vannak, akik szerint az a jó okosóra, amelyik teljesen képes kiváltani a telefont és önálló eszközként is megállja a helyét.
            Nekem úgy tűnik, hogy az Apple is ebbe az irányba ment el, amikor megpróbált minél több funkciót belezsúfolni a készülékbe.
            Vegyünk egy példát. Szerintük reális felhasználási lehetőség az, hogy az ember az okosóráján szeretne sokszáz fotót nézegetni és
            ehhez megcsináltak egy valóban nagyon ügyes és intuitív felhasználói felületet, amit a tekergetős gombbal ellátott hardver és
            az érintés erejére érzékeny kijelző is támogat. Ügyes!Az a koncepció, hogy minél több alkalmazást tudjunk az óránkra tölteni az
            alkalmazásválasztó képernyőn is megjelenik. Ezt is nagyon ötletesen, látszólag véletlenszerűen elhelyezett, de könnyedén ki-be zoomolható ikonokkal
            oldották meg azt, hogy 20-30 alkalmazás közül is kényelmesen megtaláljuk amelyiket keresünk.
            De ki akar 20-30 alkalmazást telepíteni a telefonjára? Kicsit eltúlozva a dolgokat, de idővel így fog kinézni az óránk? Persze a másik véglet a Pebble,
            ahol már 1 darab alkalmazás használata is kényelmetlen, mert végig kell előtte görgetni az összes gyárilag beállított opciót. Nem is lett sikeres a Pebble Store.
            Visszatérve a koncepcióra, a fejlődés iránya itt az, hogy minél önállóbb legyen az óra és viszonylag rövid időn belül akár teljesen függetlenné is válhasson a telefontól.
            Sokan úgy érvelnek, hogy ha az eszköz a viszonylag magas, 300-450 dolláros árkategóriába tartozik, akkor azért cserébe elvárható lenne, hogy ne csak egy félig okos,
            telefontól függő eszközt kapjanak.Ezzel az érvvel egyetértek, de szerintem maga a koncepció az, ami alapjaiban hibás. Nézzük meg, mi a másik lehetőség. </p>
    </div>

</div>
</body>

</html>
