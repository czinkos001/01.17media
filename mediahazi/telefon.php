<!DOCTYPE html>
<html lang="en">
<div class="back">
    <head>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <meta charset="UTF-8">
        <title>OkosEszközök</title>
    </head>
    <body style="background: darkgray">

    <div class="navbar-collapse">
        <?php require_once ("menu.php") ?>
    </div>
    <div class="container">
        <div class="page-header text-center">
            <h1 style="font-size: 50px">Okos telefonok</h1>
        </div>
        <div class="text-left" >


            <h2 style="font-size: 40px;color: olive">Operációs rendszer szerint</h2>
        </div>

    <div class="row">
        <div class="col-sm-8 col-lg-8 col-md-8">

            <div class="" style="border:none;">

                <div class="col-sm-6 col-md-6 col-xs-6">
                    <img src="pict/wp1.jpg" height="200px" width="200px" />
                    <img src="pict/wp2.jpg" height="200px" width="200px" />
                    <img src="pict/wp3.jpg" height="200px" width="200px" />
                </div>

                <div class="col-sm-6 col-md-6 col-xs-6">

                    <h3 style="font-size: 30px; color: #1b6d85; ">Windows Phone

                    </h3>
                    <p style="font-size:20px;">
                        A Windows Phone egy Microsoft fejlesztésű mobiltelefonos operációs rendszer, rövidítése WP. Elődje a Windows Mobile, azonban visszafele nem kompatibilis vele.
                        Ellentétben az elődjével, a Windows Phone-t elsősorban nem az üzleti, hanem az átlagfelhasználói szférának szánják. A windowsos táblagépeken nem Windows Phone,
                        hanem asztali Windows 8.1/10. A Windows Phone-t 2010 októberében kezdték el forgalmazni.
                        A legújabb verzió a Windows Phone 10 . augusztus 5-től érhető el. A Windows Phone-nal a Microsoft egy új felhasználó
                        i felületet alkotott meg, korábban Metro UI, jelenleg Modern UI néven. Ezen kívül a szoftverbe integráltak külső szolgáltatásokat és elérhetővé tettek
                        Microsoft szolgáltatásokat, valamint meghatározták a rendszert futtató telefonok minimum hardver követelményeit.

                    </p>

                </div>
                <div class="text-center">
                    <h4 style="font-size: 25px;color: orangered">Jellegzetéssegek</h4>
                 <p style="font-size:20px;">
                    <h5 style="font-size:20px;"> Az értesítési központ:</h5>
                     A  Kezdőképernyő a Windows Phone kezdőlapja. Itt 3 különböző méretben helyezhetünk el ún. "élő csempéket" (live tile). Itt találhatóak az előre telepített
                     alkalmazások (például a naptár, a telefonkönyv, a piactér, stb.) és a telepített alkalmazások, amiket a piactérről lehet letölteni. A Kezdőképernyő elemei szerk
                     eszthetőek (ide "ki lehet rögzíteni" a gombokat és a "rögzítést" fel lehet oldani), tetszőlegesen elrendezhetőek és átméretezhetőek. Ha minden elemet eltávolí
                     tunk innen, akkor a szolgáltatás eltűnik, de ha "kitűzünk" egy elemet a Startra, akkor a menü újra elérhető lesz. Van egy összesített alkalmazáslista is, ami
                     a Kezdőképernyő balra történő elgörgetésével nyitható meg. Itt megtalálható az összes alkalmazás, így a Starton csak a legszükségesebbekre van szükség
                     (természetesen az összes alkalmazást ki lehet rögzíteni a Kezdőképernyő).
                     Értesítési központ (notification center)[szerkesztés]
                 </p>
                    <p style="font-size:20px;">
                    <h5 style="font-size:20px;"> Az értesítési központ:</h5>

                        Az értesítési központ egy az Androidból átvett, a képernyő tetejéről lehúzható panel, melyben a felhasználó által kiválasztott fu
                        nkciókat lehet ki- és bekapcsolni,
                        illetve az értesítéseket megtekinteni. A Windows Phone 8.1-ben jelent meg.
                    </p> <p style="font-size:20px;">
                    <h5 style="font-size:20px;">Fizikai kezelőfelület:</h5>
                    A kijelző alatt 3 érintésérzékeny gomb van: vissza, Start és keresés. A vissza gomb az előző képernyőt jeleníti meg, a Start a kezdőképernyőre visz,
                    a keresés pedig a Bing keresőjét indítja el. A WP8.1 már támogatja, hogy ezek a gombok a képernyőn jelenjenek meg. Az USA-ban WP8.1 alatt a keresés
                    gomb nyomva tartása elindítja a Cortana személyi asszisztenst, vagy beállításoktól függően a hangvezérlést, mely itthon is működik idegen nyelven. Ezen
                    kívül a telefon oldalán 4 gomb van: hangerőszabályzó gombok, be/kikapcsoló gomb/képernyőzár, kameragomb. Eleinte a be/kikapcsoló+Start billentyűkombinációval
                    lehetett képernyőképet készíteni, viszont újonnan a be/kikapcsoló+hangerő fel billentyűkombinációval készíthetünk képet az aktuális képernyőről.
                    </p>
                </div>
            </div>

        </div>
    </div>


        <div class="row">
            <div class="col-sm-8 col-lg-8 col-md-8">

                <div class="" style="border:none;">

                    <div class="col-sm-6 col-md-6 col-xs-6">
                        <img src="pict/droid1.jpg" height="200px" width="200px" />
                        <img src="pict/droid2.jpg" height="200px" width="200px" />
                        <img src="pict/droid3.jpg" height="200px" width="200px" />
                    </div>

                    <div class="col-sm-6 col-md-6 col-xs-6">

                        <h3 style="font-size: 30px;color: greenyellow;">Android

                        </h3>
                        <p style="font-size:20px;">
                            Android Linux kernelt használó mobil operációs rendszer, elsősorban érintőképernyős mobil eszközökre (okostelefon, táblagép) tervezve.
                            Fejlesztését az Android, Inc. kezdte el, amit 2005-ben a Google felvásárolt, majd az Open Handset Alliance nevű szövetség folytatta.
                            A fejlesztők Java nyelven írhatnak rá menedzselt kódot, az eszközt a Google által fejlesztett Java programkönyvtárakon keresztül vezérelve.
                            Az első androidos telefon 2008 októberében jelent meg.

                        </p>

                    </div>
                    <div class="text-center">
                        <h4 style="font-size: 25px;color: orangered">Google árnyékában</h4>
                        <p style="font-size:20px;">
                        <h5 style="font-size:20px;">Felhasználói szemmel</h5>
                        Manapság leginkább mobil eszközökön találkozhatunk Android operációs rendszerrel. A platform lényeges része a Google eszköztár, amely lehetővé teszi,
                        hogy a Google-fiókazonosítónk megadása után a névjegyzéket, az üzeneteinket, a beszélgetéseinket, a naptárunkat, illetve a webes fotóalbumunkat az
                        operációs rendszer szinkronban tartsa. Ha levelezni kezdünk egy új partnerrel a Gmail keretein belül, akkor az Android névjegyzékében megjelenik az
                        új név; ha egy telefonhívás után felvesszük a névjegyzékbe a hívó adatait, akkor a Gmail kapcsolataink között jelenik meg szinte azonnal, s igaz ez
                        a naptárunkra, illetve a GTalk beszélgetéseinkre is. A mobilban lévő kamera segítségével készített fotókat meg tudjuk osztani a Picasa portálon.
                        Képesek vagyunk egy Chrome leszármazott böngészővel a világhálót elérni, a mobil eszköz tudása megfelel egy átlagos böngésző tudásának
                        (2.2 verzió esetén Flash Lite 10.1 révén Flash tartalom is működik), bár az élménynek a kijelző mérete határt szab, a szöveg újratördelése és a kényelmes zoom funkciók okán mégis hasznos lehet utazás közbeni internetezésre.
                        A platform alapból támogat különféle multimédia formátumokat, legyen az zene vagy videó, ám a letölthető programok révén szinte alig van olyan média
                        fájl, amelyet ne tudnánk megszólaltatni.
                        </p>
                        <p style="font-size:20px;">
                        <h5 style="font-size:20px;">Google Play</h5>

                        Az Android további érdekessége a Google Play (vagy Play Áruház), korábbi nevén Android Market, ahol több mint 1 000 000 kategorizált alkalmazást
                        tudunk letölteni a telefonunkra vagy táblagépünkre. Vannak köztük ingyenesek és fizetősek is, illetve megtalálhatóak letölthető könyvek és zenék is.
                        Egyes országokban filmeket és eszközöket is lehet a Google Play-en keresztül vásárolni.
                        </p> <p style="font-size:20px;">
                        <h5 style="font-size:20px;">Platformról</h5>
                        AAz Android platform abból a célból született, hogy egységes nyílt forrású operációs rendszere legyen a mobil eszközöknek (és itt elsősorban az okostelefon kategóriát kell érteni, mintsem egyszerű mobiltelefonokat). Az elképzelés alapja egy Linux alapú operációs rendszer volt, amelyet úgy alakítanak át, hogy képes legyen problémák nélkül kezelni a mobil eszközök integrált hardvereit (érintőképernyő, Wi-Fi, HSDPA, Bluetooth stb.). Az első lépéseknél nem volt szó Java nyelvről, azonban a Google 2005 júliusában megvásárolta az Android nevű céget, és új irányt adott a fejlesztésnek: a Linux kernel fölé egy virtuális gép került, amely a felhasználói felület kezeléséért és az alkalmazások futtatásáért felelős.

                        Ez nem ment egyik napról a másikra, és a Google az első évben igen csöndesen dolgozott, 2007 elején kezdtek kiszivárogni olyan hírek, hogy a Google belép a mobil piacra. Az iparági pletykák végül igaznak bizonyultak, bár sok esetben túlzó állításokat és rémhíreket lehetett olvasni a híroldalakon. 2007. november 5-én az Open Handset Alliance bejelentette az Android platformot, aminek tagjai között több tucat mobil technológiában érdekelt céget megtalálunk, akik érdekeltek egy szabad és nyílt forrású platform bevezetésében.

                        Napjainkra az Android platform iránt a mobiltelefon- és a tabletgyártók izgalmát leszámítva nagy érdeklődés mutatkozik a gépjárművek fedélzeti számítógépét és navigációját szállító cégek, illetve az ipari automatizálás irányából is, hiszen minden olyan helyen kényelmes az Android, ahol alapvetően kicsi a kijelző, limitáltak az erőforrások és az adatbevitel nem egérrel és/vagy billentyűzettel történik.
                        </p>
                    </div>
                </div>

            </div>
        </div> <div class="row">
            <div class="col-sm-8 col-lg-8 col-md-8">

                <div class="" style="border:none;">

                    <div class="col-sm-6 col-md-6 col-xs-6">
                        <img src="pict/droid1.jpg" height="200px" width="200px" />
                        <img src="pict/droid2.jpg" height="200px" width="200px" />
                        <img src="pict/droid3.jpg" height="200px" width="200px" />
                    </div>

                    <div class="col-sm-6 col-md-6 col-xs-6">

                        <h3 style="font-size: 30px;color: black;">iPhone</h3>
                        <p style="font-size:20px;">
                            Az iPhone az Apple által tervezett és gyártott okostelefon. Első változatát Steve Jobs, az Apple hajdani vezetője mutatta be 2007. január 9-én,
                            és azon év július 29-én került kereskedelmi forgalomba. A legfrissebb generációja, az iPhone 7, ami 2016-ban került piacra.
                            Az iPhone-ra számtalan alkalmazást (app vagy application) tudunk letölteni az App Store-on keresztül. Az App Store 2008 közepén indult,
                            és 2012-ig több mint félmillió, az Apple által jóváhagyott alkalmazás közül válogathatunk. Az alkalmazások különböző funkciókkal rendelkeznek,
                            illetve különböző alkategóriákba sorolják őket, például játékok, Reference (ahol e-book-okat találhatunk), GPS-navigáció, közösségi szolgáltatások,
                            biztonság.

                        </p>

                    </div>
                    <div class="text-center">
                        <h4 style="font-size: 25px;color: orangered"></h4>
                        <p style="font-size:20px;">
                        <h5 style="font-size:20px;">Funkciók</h5>
                        Az iPhone ismeri a konferenciahívást, hívástartást, több hívás egyidejű kezelését, hívóazonosító kijelzést, illetve hálózati szolgáltatások és
                        iPhone funkciók egyidejű kezelését. Például ha zenehallgatás közben hívnak minket, akkor a zene elhalkul, és a hívás vége után a zene ismét felhangosodik.
                        A hangtárcsázás is támogatott (mind telefonszám, mind telefonkönyv-bejegyzés bemondásával), vagy egy zeneszám vagy lejátszási lista is indítható
                        hangvezérléssel.Az iPhone rendelkezik egy úgynevezett Vizuális Hangposta funkcióval, aminek segítségével a felhasználó láthatja
                        az aktuális hangposta üzeneteit a képernyőn, anélkül hogy be kellene tárcsáznia a hangpostájára. Ellentétben más rendszerekkel,
                        az üzenetek meghallgathatók és törölhetők tetszőleges sorrendben kiválasztva a képernyőn megjelenő lista alapján. Az AT&T, O2, T-Mobile és az Orange
                        szolgáltatók módosították a hangposta rendszerüket, hogy alkalmazkodjon az Apple új funkciójához. (Később emiatt az Apple-t és az AT&T-t is beperelte a
                        Klausner Technologies, mivel szerintük az Apple Vizuális Hangposta technológiája megsérti két bejegyzett szabadalmukat is.)
                        Az egyedi csengőhang funkciót az Egyesült Államokban 2007. szeptember 5-én jelentették be, de ez a szolgáltatás nem érhető el az összes területen,
                        ahol az iPhone-t forgalmazzák. A funkció lehetővé teszi a felhasználóknak, hogy saját maguk készítsenek csengőhangokat az iTunes-on keresztül vásárolt
                        zenékből, plusz díj fizetése mellett. A csengőhang hossza 3 és 30 másodperc közötti lehet, felerősíthető és elhalkulhat, illetve fél és öt másodperc
                        közötti loopok szerkeszthetők benne. Minden szerkesztés megoldható az iTunes segítségével, a szinkronizált csengőhangok figyelmeztető jelzésként
                        is használhatók a telefonon. Egyéni csengőhangok szintén létrehozhatók az Apple GarageBand 4.1.1 vagy későbbi verziójú szoftverével (Mac OS X-re érhető
                        el egyelőre csak) és külső fejlesztők által készített programokkal is.
                        Az Apple jó néhány videót tett fel az iPhone honlapjára, amelyen keresztül a telefon különböző képességeit mutatja be.
                        </p>
                        <p style="font-size:20px;">
                        <h5 style="font-size:20px;">Alkalmazások</h5>
                        A főképernyőn iOS 7 esetén a következő alkalmazások találhatók alapból: Üzenetek, Naptár, Képek, Kamera, Videók, Térképek, Időjárás, Passbook, Jegyzetek, Emlékeztetők, Óra, Újságos, iTunes Store, App Store, Game Center, Beállítások és a FaceTime. Négy másik alkalmazás pedig még a képernyő alján, a Dock-on: Telefon, Mail, Safari és Zene.

                        A 2007-es WWDC-n, június 11-én az Apple bejelentette, hogy az iPhone támogatni fogja a külső alkalmazásokat is a Safari böngészőn keresztül, amely osztozik az iPhone interfészének kinézetével. 2007. október 17-én Steve Jobs egy nyílt levélben bejelentette, hogy 2008 februárjától elérhetővé teszik külső fejlesztők számára is az SDK-t (Szoftver Fejlesztői Környezet). A biztonsági aggodalmak miatt, illetve a Nokia digitális aláírási rendszerét követendő, várható volt, hogy az Apple hasonló rendszert fog alkalmazni. Az iPhone SDK-t hivatalosan 2008. március 6-án jelentették be az Apple Town Hall épületében. Az SDK segítségével a natív alkalmazások fejleszthetők az iPhone és iPod Touch-hoz és az iPadekhez, illetve tesztelhetők szimulátor segítségével. Az alkalmazás futtatása az eszközökön csak azután lehetséges, hogy kifizettük az Apple Developer Connection éves díját (99 dollár). A fejlesztők jóformán bármilyen árat szabhatnak az alkalmazásukhoz, amit az App Store-on keresztül terjeszthetnek és amiből 70% részesedést kapnak, 30%-ot pedig az Apple kap. A fejlesztők ugyanakkor választhatják azt is, hogy ingyenesen elérhetővé teszik az alkalmazásukat. Az SDK azonnal elérhető volt, miközben az alkalmazásokkal várni kellett a 2.0-s szoftverfrissítésig, amit 2008. július 11-én tettek közzé.

                        Az SDK megjelenéséig kizárólag webappok voltak elérhetőek az iPhone-ra, ami ezután nagyon rövid idő alatt megfordult, és az App Store lett az elsődleges. Az alkalmazások App Store-on kívüli telepítésének lehetőségét az Apple nem támogatja, hivatalosan azt csak a fejlesztők tehetik meg a saját készülékük esetén a saját, fejlesztés alatt álló alkalmazásukkal.

                        </p> <p style="font-size:20px;">
                        <h5 style="font-size:20px;">Szoftver</h5>

                        Az iPhone operációs rendszere az iOS (korábban iPhone OS), ez fut az iPhone-on, iPod Touch-on és az iPaden egyaránt. Ugyanazon a Mach kernelen alapul mindkettő, ami a Mac OS X-ben is található. Az iOS tartalmazza a Mac OS X 10.5 verziójának "Core Animation" modulját, ami a PowerVR MBX 3D hardverrel együtt felelős a felhasználói interfészben található animációk akadásmentes lejátszásáért. Az operációs rendszer jóval kevesebb mint fél GB-ot foglal magának az eszköz háttértárából. Támogatja a később megjelenő alkalmazásokat.

                        Az iPhone-t az iTunes 7.3-as és későbbi verziójával lehet használni, ami kompatibilis a Mac OS X 10.4.10-es verziójával és a 32 bites Windows XP-vel és Vistával. Az iTunes 7.6-os kiadásában már támogatja mind a 64 bites Vista-t mind a 64 bites XP-t.

                        Az iPhone központi egysége egy ARM alapú processzor, ellentétben az Apple számítógépeiben használt x86-os illetve PowerPC processzorokkal. Ez azt jelenti, hogy az egyes alkalmazásokat nem lehet egyszerűen átmásolni a Mac OS X-ből, hanem újra kell írni és fordítani azokat az iPhone-ra. Emellett a Safari webböngésző támogatja a platformfüggetlen, AJAX-ban írt webes alkalmazásokat.
                      </p>
                    </div>
                </div>

            </div>
        </div>




    </body>
</div>
</html>
