<!DOCTYPE html>
<html  lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <meta charset="UTF-8">
    <title>OkosEszközök</title>
</head>

<body style="background:greenyellow;color: #1b6d85" >
<div class="navbar-collapse">
    <?php require_once ("menu.php") ?>
</div>
<h1 class="page-header text-center" style="font-size: 40px;font-style: italic">Tabletek</h1>
<div class="text-primary">
    <p class="text-left col-lg-6 col-sm-6 col-md-6" style="font-size: 15px">
    A táblagépek pontos definícióját nehéz megadni, mivel több cég párhuzamosan és több irányból megközelítve fejlesztett olyan modelleket évtizedeken át,
        amely részben ebbe a szegmensbe sorolható. Így meghúzni egy pontos határvonalat nem lehet. Jobb híján, mivel ezt a piaci szegmenst önállóvá és meghatározóvá
        az Apple iPad-je tette így ebből a eszközből kiindulva meghatározhatjuk, hogy ma leginkább mi tekinthető táblagépnek és mik a jellemzői.
    Az egyik legátfogóbb meghatározás a következő: a táblagép jelenleg a legnagyobb olyan kézben használható személyi számítógép, amely tényleges használat
        közben is mobilisnak tekinthető, kézben használható, speciális támasz (pl. asztal) vagy segédeszköz nélkül.
    Mérete leginkább 7–14 hüvelyk hasznos képátló közé tehető. Nyilván a mérettrend változhat idővel, de azért ennek rugalmassága behatárolt, hiszen a
        túl nagy készülék elveszíti a könnyen hordozhatóság előnyét a kisebb méretet pedig már értelmetlenné teszik a már jelenlévő okostelefonok.
    Manapság az elvárt jellemzők közé a következő tulajdonságok tartoznak:
    <ul style="padding-left: 200px;font-size: 15px" class="text-left col-lg-12 col-sm-12 col-md-12 ">
        <li> Nincs beépített billentyűzet, helyette kapacitív érintőképernyő virtuális klaviatúrával.</li>
        <li>Színes kijelző.</li>
        <li> Vezeték nélküli kapcsolat (rendszerint wi-fi).</li>
        <li>Minimális vezetékes ki- és bemenet (rendszerint audiokimenet és USB-port, ami ideális esetben töltő csatlakozás is).</li>
        <li>Több órás üzemidő (a mobilitás érdekében), és legalább 150 óra készenléti idő.</li>
        <li> Lejátszik médiafájlokat és internetböngészésre is alkalmas.</li>
        <li> Használattól függően GPS, beépített kamera(k), digitális iránytű, 3 irányú gyorsulásmérő.</li>

    </ul>
<img class="img-rounded" src="pict/tablet.jpg">
    </p>
</div>
<div class="text-success">
<ul  class="list-group" style="padding-left: 70px">
    <h1 style="font-style: oblique">Csoportosítás</h1>
    <h2>Fejlesztés alapú csoportosítás</h2>
    <li >Slate táblagép (X86-alapú) lényegében PC-ből fejlesztett kompakt eszköz. Ilyen volt a Microsoft 2011-ben bejelentett gépe. Az érintőképernyővel rendelkező notebookokat hívják így, mint pl. a Lenovo Yoga nevű gépe.</li>
    <li >Mobilból fejlesztett (ARM-alapú) PC irányába. Ilyen az iPad. Itt a készülék képességei korlátozottabb, mint a tablet PC esetében, de ez nem feltétlenül csak a hardver miatt fordulhat elő, hanem üzletpolitikailag is limitálhatja a gyártó által biztosított, egyes PC-k esetén megszokott funkciókat, mint pl. a szabad egyéni szoftvertelepítést. Tulajdonképpen nagyra nőtt mobilok ezek, mivel a hardverük (és sok esetben a szoftverük is) nem tér el lényegesen egy átlagos mai okostelefontól. (Többek között az Android, BlackBerry, Windows RT, webOS, MeeGo és iOS alapú gépek tartoznak ide.)</li>
    <li >Tablet PC vagy Hibrid (X86-alapú). Szintén PC-ből fejlesztett, lecsatolható billentyűzettel, vagy billentyűzet nélkül forgalmazott gép. Ilyen volt a Lenovo X61 Tablet. A Windows 8 alapú tábla PC-k mindegyike ide tartozik (a Windows RT viszont nem).</li>
    <h2>Adatkapcsolat szerinti csoportosítás</h2>
    <li>Helyi vezeték nélküli kapcsolat. Ez a táblagépek alapváltozata, ahol a gép önmagától helyi Bluetooth, NFC, Wi-Fi, vagy egyéb hasonló kapcsolattal képes az internethez (és más eszközökhöz is) kapcsolódni, akár a notebookok. Ezek a helyi hálózatok rendszerint ingyenesek, de csak rövidtávú hatósugárral rendelkeznek, például egy lakásban, irodában, bevásárlóközpontban, így az ilyen gépek is csak ott használhatóak magukban. Közvetlen mobilinternetes hálózathoz általában nem tudnak kapcsolódni, mert az ahhoz szükséges modemek telepítőprogramjai nem kompatibilisek a táblagépek operációs rendszereivel.</li>
    <li>Mobilinternetes kapcsolat. Ezek a gépek az előbbi Wi-Fi és egyéb helyi hálózatok mellett képesek önmaguktól a 3G, 4G mobilinternethez is kapcsolódni, akár az okostelefonok, épp emiatt ilyen táblagépek kisebb méretű modelljei akár telefonként is funkcionálhatnak. Ennek megfelelően a legismertebb ismertetőjegyük a rajtuk lévő SIM-foglalat, ami az előző változatokról hiányzik. Ezek a mobilinternetes táblagépek már bárhol használhatóak, viszont a csak Wi-Fi-s gépekhez képest jelentősen drágábbak lehetnek.</li>
</ul>
</div>
<div  class="col-md-6 col-sm-6 col-lg-6 text-warning">

    
     A táblagépek mára olyan tömegáruvá váltak, amely átlagotthonban is jelen lehet, de már most nagy az ipari érdeklődés is a benne rejlő lehetőségek miatt. Főleg az
     egészségügyben tűnik hasznosnak a hordozható, szöveget, képet, videót megjelenítő színes nagy képernyő méretű eszköz.
     A másik fontos hasznosítási terület az oktatásé lehet, de ez a szektor meglehetősen árérzékeny. Mindenesetre Indiában bejelentették az oktatásra szánt
     leegyszerűsített modellt, amely állami támogatással 35 dollárért lesz elérhető a diákoknak. Ennek a készüléknek a teljes ára nagyjából 60 dollár lesz
     a tervek szerint Indián belül. Érdekes és biztató eredményt hozott a „One Laptop Per Child” szervezet kísérlete a táblagépek oktatásban való hasznosságáról. Két -
     modern civilizációtól elzárt - etióp faluban osztottak szét Motorola Xoom táblagépeket az analfabéta gyerekek között. A táblagépekre előtte oktató programokat,
     e-könyveket és filmeket telepítettek. Az egyetlen műszaki segítség, amit adtak, a napelemes töltők voltak, amelyek használatát megmutatták a felnőtteknek. Egy kutató
     hetente egyszer meglátogatta a gyerekeket, hogy felmérje a fejlődésüket. Néhány hónap alatt figyelemre méltó eredményeket mutattak. Volt például olyan gyermek, aki kívülről
     tudta az „ABC” dalt, vagy egyes szavakat le tudott írni. A legmeglepőbb mégis az volt, hogy 5 hónap után a gép bizonyos letiltott funkcióit feltörték a gyerekek, akik
     számítástechnikai eszközt a kísérlet előtt nem használtak.</div>
<img  class="img-circle"  src="pict/tablet2.jpg">
</body>

</html>
