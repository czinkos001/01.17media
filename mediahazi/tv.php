<!DOCTYPE html>
<html  lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <meta charset="UTF-8">
    <title>OkosEszközök</title>
</head>
<body class="container-fluid" style="background: #c8e5bc">
<div class="nav-tabs">
    <?php  require_once("menu.php")?>
</div>

<h1 class="panel-heading ">OkosTelevízió</h1>
<table class="table-bordered table table-responsive" style="background: #ff9d19">
    <tr>
        <th>Definíció</th>
        <th>Technológia</th>
        <th>Közösségi hálózat</th>
    </tr>
    <tr>
        <td>
            A Smart TV, olyan televízió, amely a hagyományos TV-hez képest továbbfejlesztett képességekkel rendelkezik a kapcsolódás, tartalom és felhasználói élmény tekintetében.
            Smart TV készülék lehet egy integrált internetes funkciókkal rendelkező televízió vagy egy képernyőn megjelenő menürendszerrel rendelkező televízióhoz tartozó set-top
            box, amely a hagyományos televíziókészülékekhez képest sokkal fejlettebb műveleteket és csatlakoztathatóságot tesz lehetővé. A Smart TV-ket felfoghatjuk egyfajta
            informatikai készülékeknek, vagy egy kézi számítógép számítástechnikai rendszerének, amelyet egy televízióval integráltak. A Smart TV ezzel lehetővé teszi a
            felhasználók számára, hogy egy adott platformhoz tartozó további alkalmazásokat és pluginokat/addonokat telepítsenek és futtassanak.
        </td>
        <td>
            A Smart TV szolgáltatást elérhetővé tevő technológiát televíziókészülékekbe, set-top boxokba Blu-ray lejátszókba, játékkonzolokba és az ezekhez kapcsolódó készülékekbe
            építik be. Az ilyen készülékek segítségével a felhasználók videókra, filmekre, fényképekre és egyéb internetes, helyi kábeltévé csatornán, műholdas csatornán
            vagy merevlemezen található tartalmakra kereshetnek rá.A Smart TV-k operációs rendszert vagy mobil operációs rendszert futtatnak, amely megfelelő platformot kínál
            az alkalmazásfejlesztők számára.A Smart TV még a kezdeti stádiumban jár, és folyamatosan jelennek meg hozzá olyan szoftver-keretrendszerek, mint amilyen a Google TV és
            a nyílt forráskódú XBMC platform, amelyek nagy érdeklődést keltenek a szórakoztató elektronikai piaccal foglalkozó médiában.Magyarországon elsőként a
            Samsung Electronics vezette be 2011-ben Smart TV modelljeit, melyek képesek számítógép közbeiktatása nélkül is az internetes tartalmak megjelenítésére,
            így a kedvenc weboldalak TV-re optimalizált formában olvashatók a készülék képernyőjén. Továbbá az okostévékre a fényképezőgéptől kezdve a külső merevlemezeken,
            SSD-ken, blu-ray olvasókon vagy akár notebookokon át a mobiltelefonokig számtalan készülék csatlakoztatható kábellel vagy anélkül, és a
            rajtuk tárolt tartalom akár egy okostelefon távirányítóként vagy billentyűzetként történő használatával is elérhető.
            A Samsung 2011-es Smart TV-i az alábbi funkciókat kínálják:
            A "Search All" funkcióval egyszerűen kereshetők a TV-n vagy más DLNA képes készüléken (pl. notebook, háttértároló, mobil eszközök, fényképezőgép, videokamera stb.)
            tárolt tartalmak.A „Web Browser", azaz a beépített böngésző révén bármelyik weboldal szabadon meglátogatható közvetlenül a készülékről (D6500-as LED TV-től felfelé).
            A „Samsung Apps", a világ első HDTV-re fejlesztett alkalmazásboltja ingyenes és fizetős tartalmakat, köztük magyar alkalmazásokat kínál a sport, szórakoztatás, játék,
            illetve a tudomány területén.
        </td>
        <td>Számos Smart TV platform már gyárilag vagy a felhasználó által történt bővítés hatására rendelkezik közösségi hálózati funkciókkal, amelyekkel a felhasználók letölthetik a
            legfrissebb tartalmakat a közösségi oldalakról és maguk is posztolhatnak akár az éppen nézett tartalommal kapcsolatban is.A Samsung Smart Tv modelljein widgetek-en keresztül
            érhető el a népszerű YouTube videómegosztó és a Facebook közösségi oldal, illetve az alkalmazásokon keresztül az adott weboldalak tévére optimalizált formában is
            megjeleníthetők. Természetesen a tartalom igény szerint tovább bővíthető a Samsung Appsból letölthető app-ekkel melyet a Samsung magyar képviselete folyamatosan bővít a
            hazai tartalmakkal (pl. Önkormányzati Tv, Port.hu, Index.hu, Origo.hu).
        </td>
    </tr>

<</table>
</body>

</html>
