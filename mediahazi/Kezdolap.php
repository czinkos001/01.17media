<!DOCTYPE html>
<html  lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <meta charset="UTF-8">
    <title>OkosEszközök</title>
</head>

<body style="background:gray" >
<div class="jumbotron" style="background: #5f37ff;font-size: 50px;"  >
    Okos Eszközök
</div>
<div class="navbar">
    <?php require_once ("menu.php") ?>
</div>
<div class="container">
    <div class="text-center" >
        <h1>Rövid ismertető</h1>
        <h2 class="text-info">Okostelefonok</h2>

            <div>
            <img src="pict/telefon.jpg" class="img-rounded img-thumbnail  hidden-xs" height="200px" width="200px">
                <p class="p">
                    Okostelefonnak (angolul smartphone) nevezzük a fejlett, gyakran PC-szerű funkcionalitást nyújtó mobiltelefonokat.
                    Nincs egyértelmű meghatározás arra, hogy mi az okostelefon. Egyesek szerint okostelefon az a mobil, aminek teljes értékű
                    operációs rendszere szabványosított interface-eket és platformot nyújt az alkalmazásfejlesztők számára. Mások meghatározásában az
                    okostelefon egyszerűen egy olyan készülék, ami olyan fejlett funkciókat tartalmaz, mint e-mail, Internet és e-könyv-olvasó, és/vagy teljes
                    értékű billentyűzet, vagy külső USB-s billentyűzet és VGA csatlakozó. Más szavakkal, egy olyan miniatűr számítógép, ami telefonként is képes működni.
                     Az okostelefon szó továbbgondolásával megjelent a fejlett képességekkel nem rendelkező készülékekre a „butatelefon” elnevezés.
                    A képességeiben e kettő között álló telefonokat angol nyelvterületen feature phone-nak (magyarul elterjedtebb néven: középkategóriás telefonnak) is nevezik.
                </p>
            </div>
        <h2 class="text-info">Tabletok</h2>
        <div>
            <img src="pict/tablet.png" class="img-rounded img-thumbnail  hidden-xs" height="200px" width="200px">
            <p class="p">
                táblagép vagy tablet PC hordozható számítógép, amelyet leginkább tartalomfogyasztásra fejlesztettek ki. Ezeknek az eszközöknek a legfeltűnőbb jellegzetessége a lapos,
                palatáblára emlékeztető formai kialakítás és méretarányok és ezzel együtt az igen nagy kijelzőfelület, amely az eszköz előlapjának több mint 75%-át is elfoglalhatja
                (pl. 12×16 cm-es képernyő és 13×19 cm-es keret); ez a kialakítás a felhasználói élmény növelését hivatott fokozni, főleg az audiovizuális tartalmak esetén. Hátránya azonban
                , hogy a kezelhetőséget nehezítik a hiányzó beviteli perifériák, pl. tartalomgyártás és szerkesztés esetén. Lényegében tulajdonságai és mérete alapján az ún. marokkészülé
                kek (PDA, okostelefonok) és a billentyűzettel rendelkező netbookok közé helyezhető. Célja a tényleges hordozhatóság megtartása mellett a kényelmes tartalom felhasználásho
                z szükséges (minél nagyobb) kijelző méret elérése. A táblagép elsődleges kezelési felülete a kijelzőként is funkcionáló érintőképernyője, ami a billentyűzettel és egérrel
                rendelkező számítógépekhez képest eltérő felhasználási, fejlesztési és vezérlési (programozási) filozófiát követel. Leegyszerűsített, táblagépekre szabott alkalmazások ál
                tal egyes alapvető használati funkciók könnyebben vezérelhetőek, mint az ún. „asztali számítógépek” esetén, azonban ezen egyszerű használati módon túllépő igény esetén a l
                ehetőségek erősen korlátozottak.A táblagépeknél ma már követelménynek tekinthetők az olyan integrált kiegészítők, mint a vezeték nélküli kapcsolatot szolgáló eszközök:
                Wi-Fi, bluetooth, mobilnet, valamint az olyan hasznos kiegészítők, mint a mikrofon, hangszóró, GPS, kamera, giroszkóp, gyorsulásmérő és a magnetométer.
            </p>
        </div>
        <h2 class="text-info">Okosóra</h2>
        <div>
            <img src="pict/smart_watch.jpg" class="img-rounded img-thumbnail  hidden-xs" height="200px" width="200px">
            <p class="p">
                Az okosóra egy számítógépesített karóra, amely az idő mutatásán kívül számos funkcióval bír, és gyakran hasonlítják a PDA-khoz. Míg a korai modellek még csak olyan
                alapfunkciókkal rendelkeztek, mint a számológép, a fordítás vagy játékok, a modern okosórák már egyfajta hordható számítógépként funkcionálnak. Sokukon működnek
                okostelefon-alkalmazások, némelyiknek mobil operációs rendszere is van és akár hordozható médialejátszó, FM rádió, audio- és videofájlok lejátszására képesek
                bluetooth headset használatával. Egyes modellek a mobiltelefonok minden funkcióját képesek használni, még hívást fogadni, kezdeményezni is lehet velük.
            </p>
        </div>
        <h2 class="text-info">Okos TV</h2>
        <div>
            <img src="pict/smart_tv.jpg" class="img-rounded img-thumbnail  hidden-xs" height="200px" width="200px">
            <p class="p">
                Az okostévé, Smart TV vagy „Connected TV” (nem tévesztendő össze az Internet TV-vel vagy a Web TV-vel) fogalom azt az új trendet jelöli, amelynek célja az internet és
                a modern televíziókészülékek illetve set-top boxok integrációja, valamint a számítógépek és a televíziókészülékek, illetve set-top boxok technológiai konvergenciája.
                A korábbi televíziókészülékekhez és set-top boxokhoz képest ezek az új készülékek sokkal inkább előtérbe helyezik az online interaktív média, az Internet TV, az over-t
                he-top tartalom és a videoletöltés valamint media streaming által kínált lehetőségeket, és kevésbé összpontosítanak a hagyományos műsorszolgáltatásra. Ez a jelenség ha
                sonlít arra, ahogyan az internet, a web widgetek és a szoftveres alkalmazások integrálódtak a modern okostelefonokban. Innen az elnevezés: Smart Phone, Smart TV.
            </p>
        </div>


    </div>

</div>
</body>

</html>
